import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'typeface-roboto';
import Button from '@material-ui/core/Button';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';


function App() {
    const trigger = useScrollTrigger();
  return (
    <div> 
        <Button variant="contained" color="primary">
        Hello World
        </Button>
    </div>
  );
}

ReactDOM.render(<App />, document.querySelector('#root'));